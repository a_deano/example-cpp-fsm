/* Test the fsm library */
/* (C) Evos Energy 2024 - License Proprietary */

#include <iostream>
#include <vector>
# include "../src/fsm.hpp"

using namespace std;

/* This could go in test.hpp but we'll keep it all together as an example */

/* The light has two states ON and OFF */
enum light_states {LIGHT_STATE_INVALID, LIGHT_STATE_ON, LIGHT_STATE_OFF};
string STATE_NAMES[] = {"INVALID STATE", "Light ON", "Light OFF"};

/* We define three events */
enum light_events {LIGHT_TURN_ON, LIGHT_TOGGLE, LIGHT_TURN_OFF, LIGHT_BAD_EVENT};

/* FWD Dec of light base class, the machine needs it*/
class light_base;

/* The light machine */
class light_machine_c: public fsm_machine_t<light_base, light_machine_c>
{
private:
    using fsm_machine_t::handleEvent;
    enum light_events thisEvent;
public:
    enum light_events getEvent() {return thisEvent;}
    void handleEvent(enum light_events);
};

/* Overloaded handler for the machines events */
void light_machine_c::handleEvent(enum light_events event) {
    /* Set the event in the state machine */
    this->thisEvent = event;
    /* Call the base class handler to drive machine */
    this->handleEvent();
}

/* Base light state class - specifics later for each instance */
class light_base: public fsm_state_base_t<light_machine_c> {
    public:
        virtual enum light_states type() { return LIGHT_STATE_INVALID; };
        void enterState(light_machine_c* light) override;
        void exitState(light_machine_c* light) override;
};

/* Generic enty and exits ... just for fun ! */
void light_base::enterState(light_machine_c* light)
{
    cout << "Entered : " << STATE_NAMES[light->getCurrentState()->type()] << "\n";
};

void light_base::exitState(light_machine_c* light)
{
    cout << "Left : " << STATE_NAMES[light->getCurrentState()->type()] << "\n";
};

/* Define and create our two states for the light - ON and OFF */
/* A class for OFF*/
class light_off: public light_base, public fsm_singleton_state_t<light_off> {
    public:
        enum light_states type() override { return LIGHT_STATE_OFF; }
        virtual void handleEvent(light_machine_c*) override;
};

/* A class for ON*/
class light_on: public light_base, public fsm_singleton_state_t<light_on> {
    public:
        enum light_states type() override { return LIGHT_STATE_ON; }
        virtual void handleEvent(light_machine_c*) override;
};

/* OFF's event handler */
void light_off::handleEvent(light_machine_c* light) {

    /* The light is OFF as we are state OFF */
    switch (light->getEvent()) {
        case LIGHT_TURN_ON:
        case LIGHT_TOGGLE:
            light->setCurrentState(light_on::getInstance());
            break;
        case LIGHT_TURN_OFF:
            cout << "Light is already OFF\n";
            break;
        default:
            __throw_invalid_argument("Off state can't handle this event ... ");
            break;
    };
};

/* On's event handler */
void light_on::handleEvent(light_machine_c* light) {

    /* The light is ON as we are state ON */
    switch (light->getEvent()) {
        case LIGHT_TURN_OFF:
        case LIGHT_TOGGLE:
            light->setCurrentState(light_off::getInstance());
            break;
        case LIGHT_TURN_ON:
            cout << "Light is already ON\n";
            break;
        default:
            __throw_invalid_argument("On state can't handle this event ... ");
            break;
    }
};

int main(int argc, char const *argv[])
{
    cout << "Entered main\n";

    /* Create an instance of the machine - states are singletons */
    class light_machine_c this_light;
    /* Initialise the initial state */
    this_light.setCurrentState(light_off::getInstance());

    cout << "Running states\n";
    /* We'll try these states in order */
    vector<enum light_events> evts = {LIGHT_TURN_ON, LIGHT_TURN_OFF, LIGHT_TOGGLE, LIGHT_TURN_ON, LIGHT_TOGGLE, LIGHT_TURN_OFF};
    for (auto& e: evts) {
        this_light.handleEvent(e);
        cout << "State is : " <<  STATE_NAMES[this_light.getCurrentState()->type()] << "\n";
    };

    cout << "States finished ... exiting\n";

    return 0;
};
