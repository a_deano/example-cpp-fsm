# Simple light on/off state machine - tests fsm


## Introduction

A simple state machine, models a simple light switch. There are two states and three events. The light can be `ON` or `OFF`, states are influenced by the events `TURN_ON`, `TURN_OFF` and `TOGGLE`.

The two states are `singleton`, `fsm_state_t` instances.

## Build

`mkdir build; cd build; cmake ../CMakeLists.txt; make; ./fsm_test`

## Outputs

```
Entered main
Entered : Light OFF
Running states
Left : Light OFF
Entered : Light ON
State is : Light ON
Left : Light ON
Entered : Light OFF
State is : Light OFF
Left : Light OFF
Entered : Light ON
State is : Light ON
Light is already ON
State is : Light ON
Left : Light ON
Entered : Light OFF
State is : Light OFF
Light is already OFF
State is : Light OFF
States finished ... exiting
Left : Light OFF
```
