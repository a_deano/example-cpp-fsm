/* FSM base class library header */
/* (C) Evos Energy 2024 - Licence Proprietary */

/* Detailed docs in ./README.md */

#pragma once

/* C++11 necessary */
static_assert(__cplusplus >= 201103L, "C++ lower than C++11 (or __cplusplus macro not supported)");

/* This an interface, implementors need to create these states */
/* Abstract class template to represent a state */
template <typename MACHINE>
class fsm_state_base_t
{
    public:
        /* Implementors need to provide this */
        virtual void handleEvent(MACHINE*) = 0;

        /* Implementors may want to override these*/
        virtual void enterState(MACHINE*) {}
        virtual void exitState(MACHINE*) {}
};

/* Create a singleton for derived states */
template <typename FSM_DERIVED>
class fsm_singleton_state_t
{
    friend FSM_DERIVED;
    /* Hide constructor */
    private:
        fsm_singleton_state_t() {};
    public:
        static FSM_DERIVED* getInstance()
        {
            /* C11 thread safe, lazy alloc */
            static FSM_DERIVED instance;
            return &instance;
        }
    /* Don't allow cloning */
    fsm_singleton_state_t(fsm_singleton_state_t &other) = delete;
    void operator=(const fsm_singleton_state_t &) = delete;
};

/* Template for a machine to run states from template above*/
/* The machine */
template <typename STATE_BASE, typename MACHINE>
class fsm_machine_t
{
private:
    STATE_BASE* current_state_i = nullptr;
public:
    fsm_machine_t() {};

    STATE_BASE* getCurrentState(void);
    void setCurrentState(STATE_BASE*);
    void handleEvent(void);
    virtual ~fsm_machine_t();
};

template <typename STATE_BASE, typename MACHINE>
inline STATE_BASE* fsm_machine_t<STATE_BASE, MACHINE>::getCurrentState()
{
    return current_state_i;
};

template <typename STATE_BASE, typename MACHINE>
void fsm_machine_t<STATE_BASE, MACHINE>::setCurrentState(STATE_BASE* NewState)
{
    if (current_state_i)
        current_state_i->exitState(static_cast<MACHINE*>(this));
    current_state_i = NewState;
    current_state_i->enterState(static_cast<MACHINE*>(this));
};

template <typename STATE_BASE, typename MACHINE>
void fsm_machine_t<STATE_BASE, MACHINE>::handleEvent()
{
    current_state_i->handleEvent(static_cast<MACHINE*>(this));
    return;
};

template <typename STATE_BASE, typename MACHINE>
fsm_machine_t<STATE_BASE, MACHINE>::~fsm_machine_t()
{
    current_state_i->exitState(static_cast<MACHINE*>(this));
    current_state_i = nullptr;
};
