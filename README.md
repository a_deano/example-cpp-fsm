# Generic event based *F*inite *S*tate *M*achine model

Minimum C++ version is C++11, the top of the header `static_assert(__cplusplus >= 201103L, ... )`. The `static_assert` was only introduced in C++11 as well, so older versions will fail this test anyway ...

C++ implementation of a simple FSM. States can be modelled as [**singleton**](https://refactoring.guru/design-patterns/singleton/cpp/example) classes, this is an important detail, [read this](#the-optional-fsm_singleton_state_tfsm_derived-template).

The `fsm_test` in the tests directory has a simple light switch state machine as an example implementation using the singleton pattern.

Implementors are free to use the templated classes for any number of FSM models, the test alone is an example of one possible model.

## The state interface `fsm_state_base_t`

### Usage

An abstract class template that describes three methods for a state that each take a pointer to a [`fsm_machine_t<STATE_BASE, MACHINE>`](#the-machine-template-class-fsm_machine_tstate_base-machine) instance as their argument. A default instance of this machine class handles calling these for it's current state.

#### The `fsm_state_base_t<MACHINE>` defines

- `enterState(MACHINE*)` as a  no-op
- `handleEvent(MACHINE*)` pure virtual, sub-classes must provided this function to implement the state transition logic.
- `exitState(MACHINE*)` again a no-op by default

Machines need to subclass this as a base class (can still be abstract) as the `STATE_BASE` for the `fsm_machine_t<STATE_BASE, MACHINE>` template to define the owning machine. Instances of each `STATE_BASE` class can then be defined and override the `handleEvent()` method that is called for the current state from the machine on each event.

#### The optional `fsm_singleton_state_t<FSM_DERIVED>` template

Implements a thread safe singletone state class `FSM_DERIVED` derived from `STATE_BASE`. The instance is lazy created on first call to `your_state_class::getInstance()` and that isntance is returned on each subsequent call. The instance is destroyed on program exit, this relies on **C++11** as a minimum.

#### Rationale and details

For states using this **singleton** template, the pattern is there is only one instance of each state class. Think of them like globals within a particular machine. The states are passed a pointer to their owning state machine when called, this is where state is stored. ***DO NOT* USE STATE CLASSES TO STORE ANY STATE**, unless you understand this pattern fully and have a great reason for doing so, then redesign your model to avoid it.

The singleton model model is used for resource efficency. The states themselves just store code, the state behaviour, not any data. This means they can be static so that a new class isn't created for each state transition. The upside is that for a model that uses a number of the same machine they can share behaviour, and transitions are more efficient as there isn't allocation/deallocation for every transition. States only use resource if their `getInstance()` method is called, but consume that resource forever more.

There are other ways of implementing this that can use dynamic memory allocation that are still singleton, but they are require exta code for thread safety. In our case, if we have defined a state machine for an application ... chances are we are probably using it. Machines can be implemented using this library without the use of the singleton model.

## The machine template class `fsm_machine_t<STATE_BASE, MACHINE>`

The machine that described by the states is described by another abstract class template. Subclass this to define a machine that will drive the states described by `STATE_BASE` sub-classed states. For a particular machine this class stores the state as it's `current_state`. Subclasses can define additional state data used by states as the states are passed a pointer to it's `machine` instance on every event.

#### Methods provided

- `void setCurrentState(STATE_BASE* newState)` - Call with a pointer to a state to change to `newState`. This is designed to be called from with the state handler methods themselves. Each state then controls it's transtion out when whatever conditions are met. This is not the only FSM model and it can be used to suit whichever pattern is used. It calls `current.exit(this)`, sets `current=new` then calls `new.enter()`.
- `STATE_BASE* getCurrentState(void)` - Returns a pointer to it's current state.
- `virtual void handleEvent(void)` - Call to drive the state machine, is equivalent to `machine.getCurrentState().handleEvent(this)`

### Usage

The simple [light state machine](test/README.md) in the test directory `test` has an example implementation.
